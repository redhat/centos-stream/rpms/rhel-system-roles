Source801: https://galaxy.ansible.com/download/ansible-posix-2.0.0.tar.gz
Source901: https://galaxy.ansible.com/download/community-general-10.4.0.tar.gz
Source902: https://galaxy.ansible.com/download/containers-podman-1.16.3.tar.gz

Provides: bundled(ansible-collection(ansible.posix)) = 2.0.0
Provides: bundled(ansible-collection(community.general)) = 10.4.0
Provides: bundled(ansible-collection(containers.podman)) = 1.16.3

Source996: CHANGELOG.rst
Source998: collection_readme.sh
